package org.grimmy;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Main extends AbstractHandler {
    public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        response.setContentType("application/x-protobuf");
        response.setStatus(HttpServletResponse.SC_OK);
        baseRequest.setHandled(true);
        PersonOuterClass.Person person = PersonOuterClass.Person.newBuilder()
                .setEmail("some@email.com")
                .setId(2)
                .setName("Name")
                .addPhone(PersonOuterClass.Person.PhoneNumber.newBuilder()
                        .setNumber("234-342")
                        .setType(PersonOuterClass.Person.PhoneType.MOBILE)
                        .build())
                .addPhone(PersonOuterClass.Person.PhoneNumber.newBuilder()
                        .setNumber("123-456")
                        .setType(PersonOuterClass.Person.PhoneType.HOME)
                        .build())
                .build();
        person.writeTo(response.getOutputStream());
    }

    public static void main(String[] args) throws Exception {
        Server server = new Server(8080);
        server.setHandler(new Main());

        server.start();
        server.join();
    }
}
